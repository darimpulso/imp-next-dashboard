import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createHashHistory } from 'history';
import { routerMiddleware, routerActions } from 'connected-react-router';
import { createLogger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import createRootReducer from '../reducers';
// Actions
import * as artifactsActions from '../actions/artifacts';
import * as authActions from '../actions/auth';
import * as commonActions from '../actions/common';
import * as dashActions from '../actions/dash';
import * as searchActions from '../actions/search';
import * as sweepstakesActions from '../actions/sweepstakes';
import type { dashboardStateType } from '../reducers/types';

const persistConfig = {
  key: 'imp-dashboard',
  storage,
  blacklist: ['artifacts']
  // blacklist: ['loggedIn', 'user'] // todo: only for production
};

const history = createHashHistory();

const rootReducer = createRootReducer(history);

const configureStore = (initialState?: dashboardStateType) => {
  // Redux Configuration
  const middleware = [];
  const enhancers = [];

  // Thunk Middleware
  middleware.push(thunk);

  // Logging Middleware
  const logger = createLogger({
    level: 'info',
    collapsed: true
  });

  // Skip redux logs in console during the tests
  if (process.env.NODE_ENV !== 'test') {
    middleware.push(logger);
  }

  // Router Middleware
  const router = routerMiddleware(history);
  middleware.push(router);

  // Redux DevTools Configuration
  const actionCreators = {
    ...artifactsActions,
    ...authActions,
    ...commonActions,
    ...dashActions,
    ...searchActions,
    ...sweepstakesActions,
    ...routerActions
  };
  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Options: http://extension.remotedev.io/docs/API/Arguments.html
        actionCreators
      })
    : compose;
  /* eslint-enable no-underscore-dangle */

  // Apply Middleware & Compose Enhancers
  enhancers.push(applyMiddleware(...middleware));
  const enhancer = composeEnhancers(...enhancers);

  // Create Store
  const persistedReducer = persistReducer(persistConfig, rootReducer);
  const store = createStore(persistedReducer, initialState, enhancer);
  
  const persistor = persistStore(store);
  
  if (module.hot) {
    module.hot.accept(
      '../reducers',
      // eslint-disable-next-line global-require
      () => store.replaceReducer(require('../reducers').default)
    );
  }

  return { store, persistor };
};

export default { configureStore, history };
