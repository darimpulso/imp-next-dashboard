import axios from 'axios';
import path from 'path';
import groupBy from 'lodash/groupBy';
import unique from 'lodash/uniqBy';
import sort from 'lodash/sortBy';
import { URL } from 'url';
import LdapAuth from 'ldapauth-fork';

const ldap = {
  url: 'ldaps://ldappxy.federated.fds:636',
  searchBase: 'dc=federated,dc=fds',
  searchFilter: '(sAMAccountName={{username}})',
  bindCredentials: 'secret',
  reconnect: true,
  tlsOptions: {
    rejectUnauthorized: false
  }
};
const authService = new LdapAuth(ldap);

export const auth = props => new Promise((resolve, reject) => {
    authService.authenticate(props.username, props.password, (err, user) => {
      if (err) {
        console.error(err);
        if (err.message.match('AcceptSecurityContext')) {
          return reject(new Error('Login error, please check your username and password'));
        }
        return reject(err);
      }
      localStorage.setItem('loggedIn', true);
      localStorage.setItem('user', JSON.stringify(user));
      return resolve(user);
    });
  });

export const logout = () => {

};

export const fetchBundleStats = async () => {
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  };
  const urlStaging = `http://ci-artifacts.devops.fds.com/artifactory/api/storage/imp-staging-v8/@imp`;
  const urlPreprod = `http://ci-artifacts.devops.fds.com/artifactory/api/storage/imp-preprod-v8/@imp`;
  const urlProd = `http://ci-artifacts.devops.fds.com/artifactory/api/storage/imp-prod-v8/@imp`;
  const urlCMP = 'http://ci-artifacts.devops.fds.com/artifactory/api/storage/macys-npm-private/@marketing-campaigns';
  
  try {
    const stagingRes = await axios.get(urlStaging, { headers });
    const preprodres = await axios.get(urlPreprod, { headers });
    const prodRes = await axios.get(urlProd, { headers });
    const cmpRes = await axios.get(urlCMP, { headers });
    
    const staging = typeof stagingRes.data === 'string' && JSON.parse(stagingRes.content) || stagingRes.data;
    const preprod = typeof preprodres.data === 'string' && JSON.parse(preprodres.content) || preprodres.data;
    const prod = typeof prodRes.data === 'string' && JSON.parse(prodRes.content) || prodRes.data;
    const cmpAll = typeof cmpRes.data === 'string' && JSON.parse(cmpRes.content) || cmpRes.data;
    
    const tags = [];
    
    if (staging && staging.children) {
      staging.children.map(c => tags.push({ title: c.uri.replace('/', ''), env: 'imp', uniqueKey: `imp-${c.uri.replace('/', '')}` }));
    }
    
    if (preprod && preprod.children) {
      preprod.children.map(c => tags.push({ title: c.uri.replace('/', ''), env: 'imp', uniqueKey: `imp-${c.uri.replace('/', '')}` }));
    }
    
    if (prod && prod.children) {
      prod.children.map(c => tags.push({ title: c.uri.replace('/', ''), env: 'imp', uniqueKey: `imp-${c.uri.replace('/', '')}` }));
    }
    
    if (cmpAll && cmpAll.children) {
      cmpAll.children.map(c => tags.push({ title: c.uri.replace('/', ''), env: 'cmp', uniqueKey: `cmp-${c.uri.replace('/', '')}` }));
    }
    
    return {
      imp: {
        staging,
        preprod,
        prod
      },
      cmp: {
        all: cmpAll
      },
      tags: sort(unique(tags, t => t.uniqueKey)).map((t, i) => Object.assign(t, { idx: i+1, key: `${i+1}${t.title}` }))
    }
  } catch (e) {
    throw new Error(e.message);
  }
};

export const fetchArtifactoryList = async (props) => {
  try {
    let envs = ['imp-staging-v8','imp-preprod-v8','imp-prod-v8','macys-npm-private'];
    switch(props.env) {
      case 'imp':
        envs = envs.filter(e => !e.match('macys-npm-private'));
        break;
      case 'cmp':
        envs = envs.filter(e => !e.match('imp-'));
        break;
      default: {} // eslint-disable-line
    }
    
    const url = `http://ci-artifacts.devops.fds.com/artifactory/api/search/artifact?name=${props.keyword}&repos=${envs.join(',')}`;
    const res = await axios.get(url, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    });

    
    const filtered = res.data && res.data.results && res.data.results.filter(r => !r.uri.match(/\.json$/));
    const parsedResults = filtered.map(r => {
      const newUrl = new URL(r.uri);
      const parsed = path.parse(newUrl.pathname);
      Object.assign(parsed, {
        url: r.uri,
        project: parsed.dir.match('imp-') && 'IMP' || 'CMP-CE'
      });
      if (parsed.project === 'IMP') {
        Object.assign(parsed, {
          environment: parsed.dir.match('staging') && 'STAGING' || (parsed.dir.match('preprod') && 'PREPROD' || 'PROD'),
          platform: 'IMP',
          order: parsed.dir.match('staging') && 1 || (parsed.dir.match('preprod') && 2 || 3),
          imp: true
        });
      } else {
        Object.assign(parsed, {
          environment: 'CMP',
          platform: 'CMP',
          order: 3,
          cmp: true
        });
      }
      
      Object.assign(parsed, {
        groupKey: `${parsed.order}`
      });
      
      const regex = /<?(?:[a-zA-Z0-9-]+)(\d.+)/gm;
      const regex2 = /<?([a-zA-Z0-9-]+)(-.+)/gm;
      const parsedArrOne = regex.exec(parsed.name);
      const parsedArrTwo = regex2.exec(parsed.name);
      if (parsedArrOne && parsedArrOne.length && parsedArrOne.length > 1) {
        Object.assign(parsed, {
          version: parsedArrOne[1]
        });
      }
      if (parsedArrTwo && parsedArrTwo.length && parsedArrTwo.length > 1) {
        Object.assign(parsed, {
          campaignName: parsedArrTwo[1]
        });
      }
      Object.assign(parsed, {
        campaignGroupKey: `${parsed.platform}: ${parsed.campaignName}`
      });
      return parsed;
    });
    
    const sortedResults = parsedResults.sort( (a, b) => a.version.localeCompare(b.version, undefined, { numeric:true }) );
    const reversedResults = sortedResults.reverse();
    const groupedByName = groupBy(reversedResults, 'campaignGroupKey');
    const ret = {};
    Object.keys(groupedByName).map(k => Object.assign(ret, {
        [k]: groupBy(groupedByName[k], 'groupKey')
      }));
    
    return ret;
    
  } catch (e) {
    return { error: e.message };
  }
  
};

export const fetchSweepstakes = ({ props, url }) => axios({
    method: 'GET',
    url: url || 'http://imp-preprod.herokuapp.com/v1/Sweepstakes',
    params: {
      filter: {
        ...props
      }
    },
    headers: {
      Accept: 'application/json'
    }
  });
