// @flow

export default function classNames(classes) {
  return Object.entries(classes)
  .filter(([key, value]) => value) // eslint-disable-line
  .map(([key, value]) => key) // eslint-disable-line
  .join(' ');
};
