import React  from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import App from './containers/App';
import routes from './constants/routes';

import HeaderComponent from './components/header/HeaderComponent';
import DashboardComponent from './components/dashboard/DashboardComponent';
import LoginContainer from './components/login/LoginComponent';
import SweepstakesContainer from './components/sweepstakes/SweepstakesReports';
import Platform from './components/platform';

class Routes extends React.PureComponent {
  render() {
    const { loggedIn } = this.props;
    return (
      <App>
        <Route render={props => loggedIn && (
          <HeaderComponent { ...props } />
        )} />
        <Switch>
          <Route exact path={routes.HOME} render={props => loggedIn ? (
            <DashboardComponent { ...props } />
          ) : (
            <LoginContainer { ...props } />
          )} />
          <Route path={routes.SWEEPSTAKES} component={SweepstakesContainer} />
          <Route path={routes.PLATFORM_DETAIL} component={Platform} />
        </Switch>
      </App>
    )
  }
}

const mapStateToProps = state => ({
    loggedIn: state.loggedIn
  });

export default connect(mapStateToProps)(Routes);
