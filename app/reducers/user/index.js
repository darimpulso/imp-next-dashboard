// @flow

import { AuthFilter } from '../../types'

const initialUserState = localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')) || {};

const userReducer = (state = initialUserState, action) => {
  switch (action.type) {
    case AuthFilter.LOGGED_IN:
      return action.payload;
    case AuthFilter.REFRESH_SESSION:
      return { ...state, ...action.payload };
    case AuthFilter.LOGOUT:
      localStorage.removeItem('loggedIn');
      localStorage.removeItem('user');
      return {};
    default:
      return state
  }
};

export default userReducer;
