import { DashFilter } from '../../types';

const initialState = {
  cmp: {
    all: {}
  },
  imp: {
    staging: {},
    preprod: {},
    prod: {}
  },
  tags: []
};

const dashReducer = (state = initialState, action) => {
  switch (action.type) {
    case DashFilter.FETCH_DASH_SUCCESS:
      return action.payload;
    default:
      return state
  }
};

export default dashReducer;
