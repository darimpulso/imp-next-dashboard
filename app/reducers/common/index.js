import moment from 'moment';
import { AuthFilter, CommonFilter } from '../../types';

const commonDefaultState = {
  loading: false,
  error: null,
  loginAttempts: 0,
  tempLock: false,
  nextLoginTimeout: null
};

const commonReducer = (state = commonDefaultState, action) => {
  switch (action.type) {
    case 'persist/REHYDRATE':
      return { ...state, error: null };
    case CommonFilter.SHOW_LOADING:
      return { ...state, loading: true, error: null };
    case CommonFilter.HIDE_LOADING:
      return { ...state, loading: false };
    case CommonFilter.ERROR:
      return { ...state, error: action.payload };
    case CommonFilter.SUCCESS:
      return { ...state, error: null };
    case AuthFilter.LOGIN_ERROR:
      return {
        ...state,
        error: action.payload,
        loginAttempts: state.loginAttempts + 1,
        tempLock: state.loginAttempts >= 8,
        nextLoginTimeout: state.loginAttempts === 8 && moment().add(2, 'minutes') || null
      };
    case AuthFilter.LOGGED_IN:
    case AuthFilter.RESET_LOCK:
      return { ...state, loginAttempts: 0, tempLock: false , nextLoginTimeout: null, error: null };
    default:
      return state
  }
};

export default commonReducer;
