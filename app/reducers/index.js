// @flow

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import common from './common';
import dash from './dash';
import artifacts from './artifacts';
import user from './user';
import loggedIn from './loggedIn';
import search from './search';


export default function createRootReducer(history) {
  return combineReducers({
    router: connectRouter(history),
    common,
    dash,
    artifacts,
    user,
    loggedIn,
    search
  });
}
