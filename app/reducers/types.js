import type { Dispatch as ReduxDispatch, Store as ReduxStore } from 'redux';

export type commonState = {
  +loading: boolean,
  +error: any | void,
  +loginAttempts: number,
  +tempLock: boolean,
  +nextLoginTimeout: Date | void
};

export type dashState = {
  +cmp: any,
  +imp: any,
  +tags: Array | void
};

export type searchState = {
  +search: any | void,
  +query: string,
  +alertOpen: boolean,
  +activeSearchItem: any | void,
  +error: any | void
};

export type dashboardStateType = {
  +common: commonState,
  +dash: dashState,
  +artifacts: any | void,
  +user: any | void,
  +loggedIn: boolean,
  +search: searchState,
  +sweepstakes: any
};

export type Action = {
  +type: string
};

export type GetState = () => dashboardStateType;

export type Dispatch = ReduxDispatch<Action>;

export type Store = ReduxStore<GetState, Action>;
