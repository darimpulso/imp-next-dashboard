import { SearchFilter } from '../../types';

const initialState = {
  search: null,
  query: '',
  alertOpen: false,
  activeSearchItem: null,
  error: null
};

const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case SearchFilter.SET_SEARCH:
      return { ...state, ...action.payload };
    case SearchFilter.CLEAR_SEARCH:
      return Object.assign({}, { ...initialState });
    default:
      return state
  }
};

export default searchReducer;
