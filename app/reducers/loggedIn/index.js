import { AuthFilter } from '../../types';

const initialLoggedInState = localStorage.getItem('loggedIn') || false;

const loggedInReducer = (state = initialLoggedInState, action) => {
  switch (action.type) {
    case AuthFilter.LOGGED_IN:
      return true;
    case AuthFilter.LOGOUT:
      return false;
    default:
      return state
  }
};

export default loggedInReducer;
