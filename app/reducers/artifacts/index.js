// @flow
import { ArtifactsFilter } from '../../types';

const initialState = {};

const dashReducer = (state = initialState, action) => {
  switch (action.type) {
    case ArtifactsFilter.FETCH_SEARCH_SUCCESS:
      return action.payload;
    default:
      return state
  }
};

export default dashReducer;
