// @flow

import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Button, Callout, Card, Checkbox, Classes, Elevation, FormGroup, InputGroup } from '@blueprintjs/core';
import { loginAction, resetLoginLock } from '../../actions/auth';

type Props = {
  error: any | void,
  lockTime: Date | void,
  tempLock: boolean,
  handleLoginReset: any
};

class LoginContainer extends React.PureComponent<Props> {
  props: Props;
  
  constructor(props) {
    super(props);
    this.state = {
      username: localStorage.getItem('username') || '',
      password: localStorage.getItem('password') || '',
      remember: localStorage.getItem('remember') && Boolean(localStorage.getItem('remember')) || false,
      endLock: null,
      loading: false
    };
    
    this.lockTime = 0;
    this.lockTimeout = null;
    
    this.onClickLogin = this.onClickLogin.bind(this);
    this.lockDuration = this.lockDuration.bind(this);
  }
  
  componentDidMount() {
    const { lockTime } = this.props;
    if (lockTime) {
      this.lockDuration();
    }
  }
  
  componentDidUpdate(prevProps) {
    const { tempLock } = this.props;
    if (prevProps.tempLock !== tempLock && !this.lockTimeout) {
      this.lockDuration();
    }
  }
  
  async onClickLogin() {
    this.setState({ loading: true });
    const { username, password, remember } = this.state;
    const { handleAuth } = this.props;
    const props = {
      username,
      password
    };
    
    await handleAuth(props);
    if (remember) {
      localStorage.setItem('remember', '1');
      localStorage.setItem('username', username);
      localStorage.setItem('password', password);
    }
  }
  
  lockDuration() {
    const { lockTime, handleLoginReset } = this.props;
    
    const now = moment();
    const end = now.to(lockTime);
    const t = (<span>{end}</span>);
    if (now.isBefore(lockTime)) {
      this.lockTimeout = setTimeout(this.lockDuration, 1000);
      this.setState({ endLock: t });
    } else {
      handleLoginReset();
      this.setState({ endLock: null });
    }
  }
  
  render() {
    const { remember, username, password, endLock, loading } = this.state;
    const { error, tempLock } = this.props;
    console.log(remember)
    return(
      <div className="loginContainer grid">
        <Card interactive={false} elevation={Elevation.TWO} className={Classes.DARK}>
          <h2>MACYS LDAP LOGIN</h2>
          <FormGroup
            label="USERNAME"
            labelFor="text-input"
            labelInfo="(required)"
          >
            <InputGroup id="text-input" placeholder="RAFCID" value={username} onChange={(ev) => this.setState({ username: ev.target.value })} />
          </FormGroup>
          <FormGroup
            label="PASSWORD"
            labelFor="password-input"
            labelInfo="(required)"
          >
            <InputGroup id="password-input" type="password" value={password} onChange={(ev) => this.setState({ password: ev.target.value })} />
          </FormGroup>
          <div className="row row-inline">
            <div className="col col-70">
              <div className="errorPlaceholder">
                {error && !tempLock &&
                <Callout className={Classes.INTENT_DANGER} title={error.error || 'Error'}>
                  {error.message}
                </Callout>
                }
                {tempLock &&
                  <Callout className={Classes.INTENT_DANGER} title="Account hard-locked">
                    Your account has been hard-locked, approximate lock duration {endLock}
                  </Callout>
                }
              </div>
            </div>
            <div className="col col-30" style={{
              alignItems: 'flex-end'
            }}>
              <FormGroup>
                <Checkbox checked={remember} onChange={() => this.setState({ remember: !remember })} label="Remember" />
              </FormGroup>
              <Button onClick={this.onClickLogin} loading={loading} fill disabled={tempLock}>Submit</Button>
            </div>
          </div>
        </Card>
      </div>
    )
  }
}

const mapStateToProps = state => ({
    error: state.common.error,
    loading: state.loading,
    loggedIn: state.loggedIn,
    loginAttempts: state.common.loginAttempts,
    tempLock: state.common.tempLock,
    lockTime: state.common.nextLoginTimeout
  });

const mapDispatchToProps = dispatch => ({
  handleAuth: props => dispatch(loginAction(props)),
  handleLoginReset: () => dispatch(resetLoginLock())
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
