import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  Alignment,
  Classes,
  Intent,
  Callout,
  Spinner,
  H3,
  Alert,
  Button,
  Colors
} from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import classNames from '../../utils/classNames';
import { fetchSweepstakesAction } from '../../actions/sweepstakes';

type Props = {
  fetch: Function,
  loading: boolean
};

class SweepstakesReportsComponent extends React.Component<Props> {
  props: Props;
  
  constructor(props) {
    super(props);
    this.state = {
      sweepstakesId: '',
      fromDate: moment().startOf('day').utc().toDate(),
      toDate: moment().endOf('day').utc().toDate(),
      type: 'perCampaign',
      limit: 20,
      environment: null
    };
    this.onFetchData = this.onFetchData.bind(this);
  }
  
  onFetchData() {
    const { fetch } = this.props;
    fetch({ ...this.state });
  }
  
  render() {
    const { loading } = this.props;
    const loadingClass = classNames({'bp3-skeleton': loading });
    return(
      <div className={classNames({
        [Classes.DARK]: true,
        dashboardContainer: true
      })} />
    )
  }
}

const mapStateToProps = state => ({
  loggedIn: state.loggedIn,
  sweepstakes: state.sweepstakes,
  loading: state.common.loading,
  error: state.common.error
});

const mapDispatchToProps = dispatch => ({
  fetch: (props = {}) => dispatch(fetchSweepstakesAction(props))
});

export default connect(mapStateToProps, mapDispatchToProps)(SweepstakesReportsComponent);
