// @flow

import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import {
  Alignment,
  Button,
  Classes, Menu,
  MenuItem,
  Navbar,
  NavbarDivider,
  NavbarGroup,
  ControlGroup,
  NavbarHeading,
  Popover,
  Intent,
  Tag,
  HTMLSelect,
  Position,
  FocusStyleManager
} from '@blueprintjs/core';
import { Suggest } from '@blueprintjs/select';
import { IconNames } from '@blueprintjs/icons';

import type { dashState } from '../../reducers/types';
import { logoutAction } from '../../actions/auth';
import { fetchArtifactData } from '../../actions/artifacts';
import { clearSearchAction, searchAction } from '../../actions/search';

type Props = {
  clearSearch: Function,
  handleLogout: Function,
  setSearch: Function,
  fetchSearch: Function,
  dash: dashState,
  search: any | void,
  user: any
};

class Header extends React.Component<Props> {
  props: Props;
  
  constructor(props) {
    super(props);
    this.state = {
      envFilter: null
    };
    
    FocusStyleManager.onlyShowFocusOnTabs();
    
    this.onClickLogout = this.onClickLogout.bind(this);
    this.onArtifactSelect = this.onArtifactSelect.bind(this);
    this.onClickResetSearch = this.onClickResetSearch.bind(this);
    this.onChangeQuery = this.onChangeQuery.bind(this);
  }
  
  renderArtifactItem(item, { handleClick, modifiers }) { // eslint-disable-line
    if (!modifiers.matchesPredicate) {
      return null;
    }
    return(
      <MenuItem
        style={{
          minWidth: 300,
          maxWidth: 300
        }}
        active={modifiers.active}
        label={<Tag minimal intent={item.env === 'imp' && Intent.SUCCESS || Intent.PRIMARY}>{item.env.toUpperCase()}</Tag>}
        key={item.key}
        text={item.title}
        onClick={handleClick}
      />
    )
  }
  
  filterArtifacts(query, item) { // eslint-disable-line
    return (
      item.title.indexOf(query.toLowerCase()) >= 0
    )
  }
  
  componentDidMount() {
    const { search, fetchSearch } = this.props;
    if (search.search && search.activeSearchItem) {
      fetchSearch({ search: true, keyword: search.activeSearchItem.title, env: search.activeSearchItem.env });
    }
  }
  
  onArtifactSelect(item) {
    const { setSearch, fetchSearch } = this.props;
    setSearch({ search: true });
    fetchSearch({ search: true, keyword: item.title, env: item.env });
  }
  
  onClickResetSearch() {
    const { clearSearch } = this.props;
    clearSearch();
  }
  
  onClickLogout() {
    const { handleLogout } = this.props;
    handleLogout();
  }
  
  onChangeQuery(str) {
    const { setSearch } = this.props;
    setSearch({ search: true, query: str || '' });
    if (!str || str && !str.length) {
      setSearch({ activeSearchItem: null, search: false });
    }
  }
  
  selectItems() {
    const { dash } = this.props;
    const { envFilter } = this.state;
    if (!dash.tags) {
      return [];
    }
    if (envFilter && envFilter !== 'ALL') {
      return dash.tags.filter(t => t.env === envFilter);
    }
    
    return dash.tags;
  }
  
  popoverContent() {
    const { user } = this.props;
    return (
      <Menu>
        <li className="bp3-menu-header"><h6 className="bp3-heading">{user.displayName}</h6></li>
        <Menu.Item icon={IconNames.LOG_OUT} text="Logout" onClick={this.onClickLogout} />
      </Menu>
    )
  }
  
  render() {
    const { search, setSearch, user } = this.props;
    return(
      <Navbar className={Classes.DARK} fixedToTop>
        <div style={{
          maxWidth: 1200,
          margin: '0 auto',
          padding: '0 20px'
        }}>
          <NavbarGroup align={Alignment.LEFT}>
            <NavbarHeading>
              <Link to="/">IMP/CMP ARTIFACTS</Link>
            </NavbarHeading>
            <NavbarDivider />
          </NavbarGroup>
          <NavbarGroup align={Alignment.LEFT}>
            <ControlGroup>
              <HTMLSelect
                options={[
                  {label: 'ALL', value: null},
                  {label: 'IMP', value: 'imp'},
                  {label: 'CMP', value: 'cmp'}
                ]}
                onChange={(ev) => this.setState({ envFilter: ev.currentTarget.value })}
                minimal
              />
              <Suggest
                inputProps={{
                  leftIcon: IconNames.SEARCH,
                  placeholder: 'Search artifact ...',
                  rightElement: (<Button icon={IconNames.CROSS} disabled={!search.activeSearchItem} onClick={this.onClickResetSearch} />)
                }}
                query={search.query}
                selectedItem={search.activeSearchItem}
                items={this.selectItems()}
                itemRenderer={this.renderArtifactItem}
                inputValueRenderer={item => item.title}
                noResults={<MenuItem disabled text="No results." />}
                onQueryChange={this.onChangeQuery}
                popoverProps={{
                  usePortal: false,
                  position: Position.BOTTOM,
                  preventOverflow: 'scrollParent',
                  style: {
                    maxHeight: 300,
                    overflowX: 'hidden'
                  }
                }}
                resetOnQuery={false}
                onActiveItemChange={(item) => setSearch({ activeSearchItem: item })}
                onItemSelect={this.onArtifactSelect}
                itemPredicate={this.filterArtifacts}
              />
            </ControlGroup>

          </NavbarGroup>
          <NavbarGroup align={Alignment.RIGHT}>
              <Popover
                usePortal={false}
                content={this.popoverContent()}
              >
                <Button icon={IconNames.USER} text={user.name} minimal />
              </Popover>
          </NavbarGroup>
        </div>
      </Navbar>
    )
  }
}

const mapStateToProps = state => ({
  dash: state.dash,
  user: state.user,
  search: state.search,
  loading: state.common.loading
});

const mapDispatchToProps = dispatch => ({
  handleLogout: () => dispatch(logoutAction()),
  fetchSearch: (props = {}) => dispatch(fetchArtifactData(props)),
  setSearch: (props) => dispatch(searchAction(props)),
  clearSearch: () => dispatch(clearSearchAction())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
