import React from 'react';
import { connect } from 'react-redux';

import {
  Alignment,
  Classes,
  Intent,
  Callout,
  Spinner,
  H3,
  Alert,
  Button,
  Colors
} from '@blueprintjs/core';
import { IconNames } from '@blueprintjs/icons';
import { fetchDashboard } from '../../actions/dash';
import classNames from '../../utils/classNames';
import { hideLoading } from '../../actions/common';
import type { commonState, dashState, searchState } from '../../reducers/types';

type Props = {
  handleHideLoading: Function,
  fetchDash: Function,
  user: any,
  search: searchState,
  loggedIn: boolean,
  dash: dashState,
  artifacts: any,
  ...commonState
};

class DashboardComponent extends React.Component<Props> {
  props: Props;
  
  constructor(props) {
    super(props);
    this.state = {
      alertOpen: false
    };
    this.fetchDashboardData = this.fetchDashboardData.bind(this);
  }
  
  componentDidMount() {
    const { handleHideLoading, loading } = this.props;
    this.fetchDashboardData();
    setTimeout(() => {
      if (loading) {
        handleHideLoading();
      }
    }, 20000);
  }
  
  fetchDashboardData() {
    const { fetchDash } = this.props;
    fetchDash();
  }
  
  
  getImpIntent(item) { // eslint-disable-line
    switch(item.environment) {
      case 'STAGING':
        return Intent.NONE;
      case 'PREPROD':
        return Intent.WARNING;
      case 'PROD':
        return Intent.SUCCESS;
      default:
        return Intent.PRIMARY;
    }
  }
  

  alertInProgress() { // eslint-disable-line
    return (
      <Alert canEscapeKeyCancel canOutsideClickCancel isOpen portalContainer={document.body}>
        Under construction .. will be ready in the next release ..
      </Alert>
    )
  }
  
  render() {
    const { loading, artifacts, search, error, dash } = this.props;
    const { alertOpen } = this.state;
    const loadingClass = classNames({'bp3-skeleton': loading });
    return(
      <div className={classNames({
        [Classes.DARK]: true,
        dashboardContainer: true
      })}>
        <div className="innerDashboardContainer">
          {artifacts.error &&
            <div className="row">
              <div className="col">
                <Callout intent={Intent.DANGER} icon={IconNames.ERROR} title="Error">
                  {artifacts.error}
                </Callout>
              </div>
            </div>
          }
          {!error && dash && !search.search &&
            <div className="grid">
              <div className="row">
                <div className="col col-50">
                  <Callout intent={Intent.NONE} className={Classes.DARK} title="IMP Staging" icon={IconNames.BOX}>
                    <div className="row row-inline">
                      <div className="col col-75">
                        <span className={loadingClass}>Packages: <strong>{dash.imp.staging && dash.imp.staging.children && dash.imp.staging.children.length}</strong></span>
                      </div>
                      <div className="col col-25">
                        <Button type="button" minimal icon={IconNames.EYE_OPEN} onClick={this.alertInProgress} />
                      </div>
                    </div>
                  </Callout>
                </div>
                <div className="col col-50">
                  <Callout intent={Intent.WARNING} className={Classes.DARK} title="IMP Pre-Production" icon={IconNames.BOX}>
                    <div className="row row-inline">
                      <div className="col col-75">
                        <span className={loadingClass}>Packages: <strong>{dash.imp.preprod && dash.imp.preprod.children && dash.imp.preprod.children.length}</strong></span>
                      </div>
                      <div className="col col-25">
                        <Button type="button" minimal icon={IconNames.EYE_OPEN} onClick={this.alertInProgress} />
                      </div>
                    </div>
                  </Callout>
                </div>
              </div>
              <div className="row">
                <div className="col col-50">
                  <Callout intent={Intent.SUCCESS} className={Classes.DARK} title="IMP Production" icon={IconNames.BOX}>
                    <div className="row row-inline">
                      <div className="col col-75">
                        <span className={loadingClass}>Packages: <strong>{dash.imp.prod && dash.imp.prod.children && dash.imp.prod.children.length}</strong></span>
                      </div>
                      <div className="col col-25">
                        <Button type="button" minimal icon={IconNames.EYE_OPEN} onClick={this.alertInProgress} />
                      </div>
                    </div>
                  </Callout>
                </div>
                <div className="col col-50">
                  <Callout intent={Intent.PRIMARY} className={Classes.DARK} title="CMP/CE" icon={IconNames.BOX}>
                    <div className="row row-inline">
                      <div className="col col-75">
                        <span className={loadingClass}>Packages: <strong>{dash.cmp.all && dash.cmp.all.children && dash.cmp.all.children.length}</strong></span>
                      </div>
                      <div className="col col-25">
                        <Button type="button" minimal icon={IconNames.EYE_OPEN} onClick={this.alertInProgress} />
                      </div>
                    </div>
                  </Callout>
                </div>
              </div>
            </div>
          }
          {search.search && artifacts && !artifacts.error &&
            <div className="grid">
              {artifacts.error &&
              <Callout intent={Intent.DANGER} icon={IconNames.ERROR} title="Error fetching artifacts">
                {artifacts.error}
              </Callout>
              }
              
              {loading ? (
                <Spinner className="inlineLoading" intent={Intent.PRIMARY} size={Spinner.SIZE_STANDARD} tagName="div" />
              ) : (
                <div>
                  {Object.keys(artifacts).map((k) =>
                    <div className="row" key={k}>
                      <div className="col">
                        <div className="row">
                          <H3 className={Classes.HEADING} style={{
                            color: Colors.WHITE,
                            padding: '0 10px',
                            margin: 0,
                            fontWeight: 300
                          }}>{k.toUpperCase()}</H3>
                        </div>
                        <div className="row">
                          {Object.keys(artifacts[k]).map((sk) =>
                            <div className="col" key={sk} style={{
                              order: artifacts[k][sk][0].order,
                              flex: 1,
                              maxWidth: '33.3333333333%'
                            }}>
                              <Callout
                                title={artifacts[k][sk][0].environment}
                                intent={artifacts[k][sk][0].imp && this.getImpIntent(artifacts[k][sk][0]) || Intent.PRIMARY}
                                icon={IconNames.FLOW_BRANCH}
                                className={Classes.DARK}>
                                <div className="row" style={{
                                  justifyContent: 'space-between',
                                  alignItems: 'center'
                                }}>
                                  <h3>v{artifacts[k][sk][0].version}</h3>
                                  {(artifacts[k][sk][0].environment === 'PREPROD' || artifacts[k][sk][0].environment === 'CMP') &&
                                  <div>
                                    {((artifacts[k][sk][0].environment === 'PREPROD' && artifacts[k][3] && artifacts[k][3][0].version !== artifacts[k][2][0].version) || artifacts[k][sk][0].environment === 'CMP') &&
                                      <Button
                                        align={Alignment.RIGHT}
                                        text="Sign off"
                                        minimal
                                        type="button"
                                        icon={IconNames.GIT_MERGE}
                                        onClick={() => this.setState({ alertOpen: true })}
                                      />
                                    }
                                    <Alert
                                      onCancel={() => this.setState({ alertOpen: false })}
                                      onConfirm={() => this.setState({ alertOpen: false })}
                                      canEscapeKeyCancel
                                      canOutsideClickCancel
                                      cancelButtonText="Cancel"
                                      isOpen={alertOpen}
                                      portalContainer={document.body}>
                                      Under construction .. will be ready in the next release ..
                                    </Alert>
                                  </div>
                                  }
                                </div>
                              </Callout>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  )}
                </div>)
              }
            </div>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
  search: state.search,
  loggedIn: state.loggedIn,
  dash: state.dash,
  artifacts: state.artifacts,
  loading: state.common.loading,
  error: state.common.error
});

const mapDispatchToProps = dispatch => ({
  fetchDash: (props = {}) => dispatch(fetchDashboard(props)),
  handleHideLoading: () => dispatch(hideLoading())
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardComponent);
