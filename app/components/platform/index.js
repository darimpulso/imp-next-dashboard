import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

class Platform extends React.PureComponent {
  render() {
    return(
      <div><h1>FOO</h1></div>
    )
  }
}

const mapStateToProps = state => ({
  dash: state.dash
});

export default withRouter(connect(mapStateToProps)(Platform));
