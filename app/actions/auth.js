import { AuthFilter } from '../types';
import { auth } from '../service/api';
import { hideLoading, showLoading } from './common';

const loginSuccess = payload => ({
  type: AuthFilter.LOGGED_IN,
  payload
});

const loginError = e => ({
  type: AuthFilter.LOGIN_ERROR,
  payload: e
});

const logout = () => ({
  type: AuthFilter.LOGOUT
});

export const loginAction = props => dispatch => {
  dispatch(showLoading());
  return auth(props)
  .then(res => dispatch(loginSuccess(res)))
  .catch(e => dispatch(loginError(e)))
  .finally(() => dispatch(hideLoading()));
};

export const logoutAction = () => dispatch => {
  dispatch(showLoading());
  dispatch(logout());
  dispatch(hideLoading());
  return true;
};

export const resetLoginLock = () => ({
  type: AuthFilter.RESET_LOCK
});
