// fetchSweepstakesAction


import { SweepsFilter, CommonFilter } from '../types';
import { fetchSweepstakes } from '../service/api';
import { hideLoading, showLoading } from './common';

const fetchSuccess = payload => ({
  type: SweepsFilter.FETCH_REPORT_SUCCESS,
  payload
});

const fetchError = e => ({
  type: CommonFilter.ERROR,
  payload: e
});


export const fetchSweepstakesAction = props => dispatch => {
  dispatch(showLoading());
  return fetchSweepstakes(props)
  .then(res => dispatch(fetchSuccess(res)))
  .catch(e => dispatch(fetchError(e)))
  .finally(() => dispatch(hideLoading()));
};
