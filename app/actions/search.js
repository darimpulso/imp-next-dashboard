// fetchDashboard

import { SearchFilter} from '../types';

export const searchAction = payload => ({
  type: SearchFilter.SET_SEARCH,
  payload
});

export const clearSearchAction = () => ({
  type: SearchFilter.CLEAR_SEARCH
});
