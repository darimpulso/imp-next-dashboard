import { CommonFilter } from '../types';

export const showLoading = () => ({
  type: CommonFilter.SHOW_LOADING
});

export const hideLoading = () => ({
  type: CommonFilter.HIDE_LOADING
});

export const handleError = props => ({
  type: CommonFilter.ERROR,
  payload: props.data
});

export const handleSuccess = props => ({
  type: CommonFilter.SUCCESS,
  payload: props.data
});
