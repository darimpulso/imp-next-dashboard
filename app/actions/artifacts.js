// fetchArtifactsOverview

import { ArtifactsFilter, CommonFilter } from '../types';
import { fetchArtifactoryList } from '../service/api';
import { hideLoading, showLoading } from './common';

const fetchSuccess = payload => ({
  type: ArtifactsFilter.FETCH_SEARCH_SUCCESS,
  payload
});

const fetchError = e => ({
  type: CommonFilter.ERROR,
  payload: e
});


export const fetchArtifactData = props => dispatch => {
  dispatch(showLoading());
  return fetchArtifactoryList(props)
  .then(res => dispatch(fetchSuccess(res)))
  .catch(e => dispatch(fetchError(e)))
  .finally(() => dispatch(hideLoading()));
};
