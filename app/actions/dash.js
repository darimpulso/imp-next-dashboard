// fetchDashboard

import { DashFilter, CommonFilter } from '../types';
import { fetchBundleStats } from '../service/api';
import { hideLoading, showLoading } from './common';

const fetchSuccess = payload => ({
  type: DashFilter.FETCH_DASH_SUCCESS,
  payload
});

const fetchError = e => ({
  type: CommonFilter.ERROR,
  payload: e
});


export const fetchDashboard = props => dispatch => {
  dispatch(showLoading());
  return fetchBundleStats(props)
  .then(res => dispatch(fetchSuccess(res)))
  .catch(e => dispatch(fetchError(e)))
  .finally(() => dispatch(hideLoading()));
};
